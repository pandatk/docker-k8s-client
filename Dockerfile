FROM alpine:3.8

ARG BUILD_DATE
ARG VERSION
ARG VCS_REF

RUN apk add --update ca-certificates \
    && apk add --update -t deps curl openssl bash \
    && apk add --update gettext \
    && curl -L https://storage.googleapis.com/kubernetes-release/release/${KUBE_LATEST_VERSION}/bin/linux/amd64/kubectl -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl \
    && curl https://raw.githubusercontent.com/helm/helm/master/scripts/get > get_helm.sh \
    && chmod 700 get_helm.sh \
    && ./get_helm.sh \
    && apk del --purge deps bash \
    && rm /var/cache/apk/*



